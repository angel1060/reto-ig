import 'package:retoig/src/pages/album_page.dart';
import 'package:flutter/material.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'RetoIgApp',
      debugShowCheckedModeBanner: false,
      home: AlbumPage(),

    );
  }
}