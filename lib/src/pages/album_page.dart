import 'package:retoig/src/models/album_model.dart';
import 'package:retoig/src/providers/albums_provider.dart';
import 'package:flutter/material.dart';


class AlbumPage extends StatelessWidget {
    final albumsProvider = new AlbumsProvider();

    @override
    Widget build(BuildContext context) {

      return Scaffold(
        appBar: AppBar(
          title: Text('Mis Fotografias')
        ),
        body: _loadAlbums(),
      );
        
    }

    Widget _loadAlbums() {
        return FutureBuilder (
            future: albumsProvider.loadAlbumgs(),
            builder: (BuildContext context, AsyncSnapshot<List<AlbumModel>> snapshot){
              if(snapshot.hasData){
                final albums = snapshot.data;

                return ListView.builder(
                  itemCount: albums.length,
                  itemBuilder: (context, i) => _buildItem(albums[i], context),
                );

              }else{
                return Center(child: CircularProgressIndicator());
              }
            }
        );
    }

    _buildItem(AlbumModel album, BuildContext context){

        return Card(
          color: Colors.white,
          child: Column(
            children: <Widget>[
                ListTile(
                    title: Text('${ album.title }', style: TextStyle(color: Colors.black)),
                    subtitle: Text( 'Album: ' + album.albumId.toString() ),
                    leading: ( album.thumbnailUrl == null ) 
                      ? Image(image: AssetImage('assets/no-image.png')) 
                      : Image(image: NetworkImage(album.thumbnailUrl)),
                    trailing: Icon( Icons.leak_add),
                    onTap: () => _showAlertImage(context, album.url),
                )
            ],
          )
        );
    }

    _showAlertImage(BuildContext context, String imageUrl){
        showDialog(
          context: context,
          barrierDismissible: true,
          builder: (context){
            return AlertDialog(
              backgroundColor: Colors.transparent,
              
              content: ( imageUrl == null ) 
                      ? Image(image: AssetImage('assets/no-image.png')) 
                      : FadeInImage(
                        image: NetworkImage( imageUrl ),
                        placeholder: AssetImage('assets/loading.gif'),
                        fit: BoxFit.cover
                      )
            );
          }
        );
    }
}
    
  