
import 'dart:convert';

import 'package:retoig/src/models/album_model.dart';
import 'package:http/http.dart' as http;

class AlbumsProvider {
   final String _url = 'https://jsonplaceholder.typicode.com/photos';

  Future<List<AlbumModel>> loadAlbumgs() async {
      final resp = await http.get(_url);
      final  List<dynamic> decodedData = json.decode(resp.body);
      final List<AlbumModel> albums = new List();

      if( decodedData != null ) {
        decodedData.forEach( (data){
          albums.add(AlbumModel.fromJson(data));
        });
      }
      return albums;
  }
  
}
